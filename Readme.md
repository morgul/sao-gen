# SAO Text Generator.

Just a little project to amuse myself by harassing my TBC playing friends.

## But the font isn't right

I build this in a single evening, and got tired of looking at fonts after page 8 or so. If anyone knows the correct font, let me kknow.

## Where's all the animations, or the background, or...

An evening. One. Uno. I might got back and add some nifty random animations and such, but this is close enough for now.

## Dude, this is cool, can I buy you a coffee?

Nope. But you know what you can do? Donate to my friend's Extra Life campign:

https://www.extra-life.org/index.cfm?fuseaction=donorDrive.team&teamID=50655

(It benefits Children's Miracle Network Hospitals. Great cause.)
